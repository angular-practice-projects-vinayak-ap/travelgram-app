import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

//Servics
import { AuthService } from 'src/app/services/auth.service';

//Forms
import { NgForm } from '@angular/forms';

import { finalize } from 'rxjs/operators';

//Firebase
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

//Browser Image Resizer
import { readAndCompressImage } from 'browser-image-resizer';
import { imageConfig } from './../../../utilities/config';

//uuid
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  picture: string =
    'https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png';
  uploadPercent: number = null;

  constructor(
    private auth: AuthService,
    private router: Router,
    private database: AngularFireDatabase,
    private storage: AngularFireStorage,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  onSubmit(myForm: NgForm) {
    const {
      email,
      password,
      instaHandle,
      country,
      bio,
      name,
    } = myForm.form.value;

    this.auth
      .signUp(email, password)
      .then((res) => {
        console.log(res);
        const { uid } = res.user;

        this.database.object(`/users/${uid}`).set({
          id: uid,
          name: name,
          email: email,
          instaUserName: instaHandle,
          country: country,
          bio: bio,
          picture: this.picture,
        });
      })
      .then(() => {
        this.router.navigateByUrl('/');
        this.toastr.success('You have successfully signed up');
      })
      .catch((err) => {
        this.toastr.error(err.message, 'Sign Up Failed');
      });
  }

  async uploadFile(event) {
    const file = event.target.files[0];

    let resizedImage = await readAndCompressImage(file, imageConfig);

    const filePath = uuidv4();
    const fileRef = this.storage.ref(filePath);

    const task = this.storage.upload(filePath, resizedImage);

    task.percentageChanges().subscribe((percetage) => {
      this.uploadPercent = percetage;
    });

    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.picture = url;
            this.toastr.success('Image uploaded successfully');
          });
        })
      )
      .subscribe();
  }
}
