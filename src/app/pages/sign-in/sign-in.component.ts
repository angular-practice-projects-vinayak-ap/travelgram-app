import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSubmit(myForm: NgForm) {
    const { email, password } = myForm.form.value;

    this.auth
      .signIn(email, password)
      .then((res) => {
        this.toastr.success('You have successfully signed in', '', {
          closeButton: true,
        });
        this.router.navigateByUrl('/');
      })
      .catch((err) => {
        this.toastr.error(err.message, 'Sign In Failed', {
          closeButton: true,
        });
      });
  }
}
