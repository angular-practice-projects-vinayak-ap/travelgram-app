import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  email = null;
  // uid = null;
  // instaId = null;
  // user = null;

  constructor(
    private auth: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private database: AngularFireDatabase
  ) {
    auth.getUser().subscribe((user) => {
      console.log('USER IS:', user);
      this.email = user?.email;

      // this.uid = user?.uid;
      // console.log(this.uid);
    });

    // FOR RETRIEVING INSTA USER NAME FROM REALTIME DATABASE
    // database
    //   .object(`/users/${this.uid}`)
    //   .valueChanges()
    //   .subscribe((user) => {
    //     console.log('Recieved User', user);
    //     this.instaId = user?.instaUserName;
    //   });
  }

  ngOnInit(): void {}

  async handleSignOut() {
    try {
      await this.auth.signOut();
      this.router.navigateByUrl('/sign-in');
      this.toastr.info('You have successfully signed out', '', {
        closeButton: true,
      });
      this.email = null;
    } catch (err) {
      this.toastr.error(err.message, 'Problem in signing out', {
        closeButton: true,
      });
    }
  }
}
